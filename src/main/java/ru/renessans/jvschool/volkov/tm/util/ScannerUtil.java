package ru.renessans.jvschool.volkov.tm.util;

import ru.renessans.jvschool.volkov.tm.exception.incorrect.IndexIncorrectException;

import java.util.Scanner;

public interface ScannerUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextInteger() {
        final String line = nextLine();
        try {
            return Integer.parseInt(line);
        } catch (Exception e) {
            throw new IndexIncorrectException(line);
        }
    }

}