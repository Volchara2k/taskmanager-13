package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.repository.ICrudRepository;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyTaskException;
import ru.renessans.jvschool.volkov.tm.model.Task;

import java.util.*;

public final class TaskRepository implements ICrudRepository<Task> {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task add(final Task task) {
        this.tasks.add(task);
        return task;
    }

    @Override
    public Task removeByIndex(final Integer index) {
        final Task task = getByIndex(index);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        this.tasks.remove(task);
        return task;
    }

    @Override
    public Task removeById(final String id) {
        final Task task = getById(id);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        this.tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByTitle(final String title) {
        final Task task = getByTitle(title);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        this.tasks.remove(task);
        return task;
    }

    @Override
    public List<Task> removeAll() {
        final List<Task> tasks = new ArrayList<>(this.tasks);
        this.tasks.clear();
        return tasks;
    }

    @Override
    public Task getByIndex(final Integer index) {
        return this.tasks.get(index);
    }

    @Override
    public Task getById(final String id) {
        for (final Task task : this.tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task getByTitle(final String name) {
        for (final Task task : this.tasks) {
            if (name.equals(task.getTitle())) return task;
        }
        return null;
    }

    @Override
    public List<Task> getAll() {
        return new ArrayList<>(this.tasks);
    }

}