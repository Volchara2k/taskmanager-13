package ru.renessans.jvschool.volkov.tm.exception.empty;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyTaskException extends AbstractRuntimeException {

    private static final String EMPTY_TASK = "Ошибка! Парамерт \"задача\" является null!\n";

    public EmptyTaskException() {
        super(EMPTY_TASK);
    }

}