package ru.renessans.jvschool.volkov.tm.controller;

import ru.renessans.jvschool.volkov.tm.api.controller.ICrudController;
import ru.renessans.jvschool.volkov.tm.api.service.ICrudService;
import ru.renessans.jvschool.volkov.tm.api.view.ICrudView;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;

import java.util.List;

public final class ProjectController implements ICrudController {

    private final ICrudService<Project> projectService;

    private final ICrudView<Project> taskView;

    public ProjectController(final ICrudService<Project> projectService, final ICrudView<Project> projects) {
        this.projectService = projectService;
        this.taskView = projects;
    }

    @Override
    public void list() {
        final List<Project> projects = this.projectService.getAll();
        this.taskView.print(projects);
    }

    @Override
    public void create() {
        final String title = this.taskView.getLine();
        final String description = this.taskView.getLine();
        final Project project = this.projectService.add(title, description);
        this.taskView.print(project);
    }

    @Override
    public void updateByIndex() {
        final Integer index = ScannerUtil.nextInteger() - 1;
        final String title = this.taskView.getLine();
        final String description = this.taskView.getLine();
        final Project project = this.projectService.updateByIndex(index, title, description);
        this.taskView.print(project);
    }

    @Override
    public void updateById() {
        final String id = this.taskView.getLine();
        final String title = this.taskView.getLine();
        final String description = this.taskView.getLine();
        final Project project = this.projectService.updateById(id, title, description);
        this.taskView.print(project);
    }

    @Override
    public void removeByIndex() {
        final Integer index = ScannerUtil.nextInteger() - 1;
        final Project project = this.projectService.removeByIndex(index);
        this.taskView.print(project);
    }

    @Override
    public void removeById() {
        final String id = this.taskView.getLine();
        final Project project = this.projectService.removeById(id);
        this.taskView.print(project);
    }

    @Override
    public void removeByTitle() {
        final String title = this.taskView.getLine();
        final Project project = this.projectService.removeByTitle(title);
        this.taskView.print(project);
    }

    @Override
    public void removeAll() {
        final List<Project> projects = this.projectService.removeAll();
        this.taskView.print(projects);
    }

    @Override
    public void viewByIndex() {
        final Integer index = ScannerUtil.nextInteger() - 1;
        final Project project = this.projectService.getByIndex(index);
        this.taskView.print(project);
    }

    @Override
    public void viewById() {
        final String id = this.taskView.getLine();
        final Project project = this.projectService.getById(id);
        this.taskView.print(project);
    }

}