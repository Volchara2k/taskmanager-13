package ru.renessans.jvschool.volkov.tm.view;

import ru.renessans.jvschool.volkov.tm.api.view.ICommandView;

public final class CommandView implements ICommandView {

    @Override
    public void print(final String notify) {
        System.out.println(notify);
    }

}