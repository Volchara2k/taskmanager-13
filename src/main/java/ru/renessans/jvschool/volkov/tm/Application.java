package ru.renessans.jvschool.volkov.tm;

import ru.renessans.jvschool.volkov.tm.bootstrap.Bootstrap;

public final class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}