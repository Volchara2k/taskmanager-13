package ru.renessans.jvschool.volkov.tm.model;

import java.util.UUID;

public class Project {

    private String id = UUID.randomUUID().toString();

    private String title = "";

    private String description = "";

    public Project() {
    }

    public Project(final String title) {
        this.title = title;
    }

    public Project(final String title, final String description) {
        this.title = title;
        this.description = description;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (this.title != null)
            result.append("Заголовок проекта: ").append(title);
        if (this.description != null)
            result.append(", описание проекта: ").append(this.description);
        return result.toString();
    }

}