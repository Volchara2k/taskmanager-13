package ru.renessans.jvschool.volkov.tm.enumeration;

public enum CommandType {

    COMMON("Общая"),

    CMD("Терминальная команда"),

    ARG("Программный аргумент");

    private final String title;

    CommandType(final String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public boolean isCommon() {
        return this == COMMON;
    }

    public boolean isCommand() {
        return this == CMD;
    }

    public boolean isArgument() {
        return this == ARG;
    }

}