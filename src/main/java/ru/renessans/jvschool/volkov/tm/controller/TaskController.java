package ru.renessans.jvschool.volkov.tm.controller;

import ru.renessans.jvschool.volkov.tm.api.controller.ICrudController;
import ru.renessans.jvschool.volkov.tm.api.service.ICrudService;
import ru.renessans.jvschool.volkov.tm.api.view.ICrudView;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;

import java.util.List;

public final class TaskController implements ICrudController {

    private final ICrudService<Task> taskService;

    private final ICrudView<Task> taskView;

    public TaskController(final ICrudService<Task> taskService, final ICrudView<Task> taskView) {
        this.taskService = taskService;
        this.taskView = taskView;
    }

    @Override
    public void create() {
        final String title = this.taskView.getLine();
        final String description = this.taskView.getLine();
        final Task task = this.taskService.add(title, description);
        this.taskView.print(task);
    }

    @Override
    public void list() {
        final List<Task> tasks = this.taskService.getAll();
        this.taskView.print(tasks);
    }

    @Override
    public void updateByIndex() {
        final Integer index = ScannerUtil.nextInteger() - 1;
        final String title = this.taskView.getLine();
        final String description = this.taskView.getLine();
        final Task updatedTask = this.taskService.updateByIndex(index, title, description);
        this.taskView.print(updatedTask);
    }

    @Override
    public void updateById() {
        final String id = this.taskView.getLine();
        final String title = this.taskView.getLine();
        final String description = this.taskView.getLine();
        final Task updatedTask = this.taskService.updateById(id, title, description);
        this.taskView.print(updatedTask);
    }

    @Override
    public void removeByIndex() {
        final Integer index = ScannerUtil.nextInteger() - 1;
        final Task task = this.taskService.removeByIndex(index);
        this.taskView.print(task);
    }

    @Override
    public void removeById() {
        final String id = this.taskView.getLine();
        final Task task = this.taskService.removeById(id);
        this.taskView.print(task);
    }

    @Override
    public void removeByTitle() {
        final String title = this.taskView.getLine();
        final Task task = this.taskService.removeByTitle(title);
        this.taskView.print(task);
    }

    @Override
    public void removeAll() {
        final List<Task> tasks = this.taskService.removeAll();
        this.taskView.print(tasks);
    }

    @Override
    public void viewByIndex() {
        final Integer index = ScannerUtil.nextInteger() - 1;
        final Task task = this.taskService.getByIndex(index);
        this.taskView.print(task);
    }

    @Override
    public void viewById() {
        final String id = this.taskView.getLine();
        final Task task = this.taskService.getById(id);
        this.taskView.print(task);
    }

}