package ru.renessans.jvschool.volkov.tm.exception.empty;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyIdException extends AbstractRuntimeException {

    private static final String EMPTY_ID = "Ошибка! Парамерт \"идентификатор\" является пустым или null!\n";

    public EmptyIdException() {
        super(EMPTY_ID);
    }

}