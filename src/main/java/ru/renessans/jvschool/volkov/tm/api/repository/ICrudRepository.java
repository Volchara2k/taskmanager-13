package ru.renessans.jvschool.volkov.tm.api.repository;

import java.util.List;

public interface ICrudRepository<T> {

    T add(T object);

    T removeByIndex(Integer index);

    T removeById(String id);

    T removeByTitle(String title);

    List<T> removeAll();

    T getByIndex(Integer index);

    T getById(String id);

    T getByTitle(String title);

    List<T> getAll();

}