package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.repository.ICrudRepository;
import ru.renessans.jvschool.volkov.tm.api.service.ICrudService;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyDescriptionException;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyIdException;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyProjectException;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyTitleException;
import ru.renessans.jvschool.volkov.tm.exception.incorrect.IndexIncorrectException;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.List;
import java.util.Objects;

public final class ProjectService implements ICrudService<Project> {

    private final ICrudRepository<Project> projectRepository;

    public ProjectService(final ICrudRepository<Project> projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        final Project project = new Project(title, description);
        return this.projectRepository.add(project);
    }

    @Override
    public Project updateByIndex(final Integer index, final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IndexIncorrectException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        final Project project = getByIndex(index);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        project.setTitle(title);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateById(final String id, final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        final Project project = getById(id);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        project.setTitle(title);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IndexIncorrectException();
        return this.projectRepository.removeByIndex(index);
    }

    @Override
    public Project removeById(final String id) {
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.projectRepository.removeById(id);
    }

    @Override
    public Project removeByTitle(final String title) {
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.projectRepository.removeByTitle(title);
    }

    @Override
    public List<Project> removeAll() {
        return this.projectRepository.removeAll();
    }

    @Override
    public Project getByIndex(final Integer index) {
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IndexIncorrectException();
        return this.projectRepository.getByIndex(index);
    }

    @Override
    public Project getById(final String id) {
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.projectRepository.getById(id);
    }

    @Override
    public Project getByTitle(final String title) {
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.projectRepository.getByTitle(title);
    }

    @Override
    public List<Project> getAll() {
        return this.projectRepository.getAll();
    }

}