package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.enumeration.CommandType;
import ru.renessans.jvschool.volkov.tm.exception.unknown.UnknownCommandTypeException;
import ru.renessans.jvschool.volkov.tm.model.Command;

import java.util.*;

public final class CommandRepository implements ICommandRepository {

    private final Map<CommandType, List<Command>> commands = new HashMap<>();

    @Override
    public List<CommandType> addCommandTypes(final List<CommandType> types) {
        for (CommandType type : types) {
            this.commands.put(type, new ArrayList<>());
        }
        return types;
    }

    @Override
    public List<Command> addCommands(final CommandType commandType, final List<Command> commands) {
        final List<Command> commandList = this.commands.get(commandType);
        commandList.addAll(commands);
        return commands;
    }

    @Override
    public List<Command> getCommandsByType(final CommandType commandType) {
        if (commandType.isCommon()) return getCommandsList(CommandType.COMMON);
        if (commandType.isArgument()) return getCommandsList(CommandType.ARG);
        if (commandType.isCommand()) return getCommandsList(CommandType.CMD);
        return new ArrayList<>();
    }

    @Override
    public String getNotifyByType(final CommandType commandType, final String command) {
        final List<Command> commands = getCommandsByType(commandType);
        for (Command cmd : commands) {
            if (Objects.equals(commandByType(commandType, cmd), command))
                return cmd.getNotification();
        }
        return null;
    }

    private String commandByType(final CommandType commandType, final Command command) {
        if (commandType.isArgument()) return command.getArgument();
        if (commandType.isCommand()) return command.getCommand();
        return null;
    }

    private List<Command> getCommandsList(final CommandType commandType) {
        if (isNoCommandType(commandType)) throw new UnknownCommandTypeException();
        return commands.get(commandType);
    }

    private boolean isNoCommandType(final CommandType commandType) {
        return this.commands.get(commandType) == null;
    }
}