package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.repository.ICrudRepository;
import ru.renessans.jvschool.volkov.tm.api.service.ICrudService;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyDescriptionException;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyIdException;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyTaskException;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyTitleException;
import ru.renessans.jvschool.volkov.tm.exception.incorrect.IndexIncorrectException;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.List;
import java.util.Objects;

public final class TaskService implements ICrudService<Task> {

    private final ICrudRepository<Task> taskRepository;

    public TaskService(final ICrudRepository<Task> taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();
        final Task task = new Task(title, description);
        return this.taskRepository.add(task);
    }

    @Override
    public Task updateByIndex(final Integer index, final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IndexIncorrectException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        final Task task = getByIndex(index);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        task.setTitle(title);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateById(final String id, final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        final Task task = getById(id);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        task.setTitle(title);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IndexIncorrectException();
        return this.taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeById(final String id) {
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.taskRepository.removeById(id);
    }

    @Override
    public Task removeByTitle(final String title) {
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.taskRepository.removeByTitle(title);
    }

    @Override
    public List<Task> removeAll() {
        return this.taskRepository.removeAll();
    }

    @Override
    public Task getByIndex(final Integer index) {
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IndexIncorrectException();
        return this.taskRepository.getByIndex(index);
    }

    @Override
    public Task getById(final String id) {
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.taskRepository.getById(id);
    }

    @Override
    public Task getByTitle(final String title) {
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.taskRepository.getByTitle(title);
    }

    @Override
    public List<Task> getAll() {
        return this.taskRepository.getAll();
    }

}