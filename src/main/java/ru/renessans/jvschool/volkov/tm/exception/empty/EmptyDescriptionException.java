package ru.renessans.jvschool.volkov.tm.exception.empty;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyDescriptionException extends AbstractRuntimeException {

    private static final String EMPTY_DESCRIPTION = "Ошибка! Парамерт \"описание\" является пустым или null!\n";

    public EmptyDescriptionException() {
        super(EMPTY_DESCRIPTION);
    }

}