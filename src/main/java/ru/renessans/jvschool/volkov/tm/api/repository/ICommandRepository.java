package ru.renessans.jvschool.volkov.tm.api.repository;

import ru.renessans.jvschool.volkov.tm.enumeration.CommandType;
import ru.renessans.jvschool.volkov.tm.model.Command;

import java.util.List;

public interface ICommandRepository {

    List<CommandType> addCommandTypes(List<CommandType> types);

    List<Command> addCommands(CommandType commandType, List<Command> commands);

    List<Command> getCommandsByType(CommandType commandType);

    String getNotifyByType(CommandType commandType, String command);

}