package ru.renessans.jvschool.volkov.tm.controller;

import ru.renessans.jvschool.volkov.tm.api.controller.ICommandController;
import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;
import ru.renessans.jvschool.volkov.tm.api.view.ICommandView;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    private final ICommandView commandView;

    private static final String FORMAT_REGISTRATION_MSG = "%s успешно зарегистрированы!\n";

    public CommandController(final ICommandService commandService, final ICommandView commandView) {
        this.commandService = commandService;
        this.commandView = commandView;
    }

    @Override
    public void registrationCommandTypes() {
        this.commandService.addCommandTypes().forEach(commandType ->
                this.commandView.print(commandType.name()));
        this.commandView.print(String.format(FORMAT_REGISTRATION_MSG, "Типы команд"));
    }

    @Override
    public void registrationCommands() {
        this.commandService.addCommonCommands().forEach(command ->
                this.commandView.print(command.getCommand() + ", " + command.getArgument()));
        this.commandView.print(String.format(FORMAT_REGISTRATION_MSG, "Основные команды"));
        this.commandService.addTerminalCommands().forEach(command ->
                this.commandView.print(command.getCommand()));
        this.commandView.print(String.format(FORMAT_REGISTRATION_MSG, "Терминальные команды"));
        this.commandService.addArgumentCommands().forEach(command ->
                this.commandView.print(command.getArgument()));
        this.commandView.print(String.format(FORMAT_REGISTRATION_MSG, "Аргументы"));
    }

    @Override
    public void registrationNotifyCommands() {
        this.commandService.addNotifyCommonCommands();
        this.commandService.addNotifyTerminalCommands();
        this.commandService.addNotifyArgumentCommands();
        this.commandView.print(String.format(FORMAT_REGISTRATION_MSG, "Уведомления для отдельных списков команд"));
    }

    @Override
    public void printCommandNotify(final String command) {
        final String notify = this.commandService.getTerminalCommandNotify(command);
        this.commandView.print(notify);
    }

    @Override
    public void printArgumentNotify(final String command) {
        final String notify = this.commandService.getArgumentCommandNotify(command);
        this.commandView.print(notify);
    }

}