package ru.renessans.jvschool.volkov.tm.exception.empty;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyCommandTypeException extends AbstractRuntimeException {

    private static final String EMPTY_COMMAND_TYPE = "Ошибка! Парамерт \"тип команды\" является null!\n";

    public EmptyCommandTypeException() {
        super(EMPTY_COMMAND_TYPE);
    }

}