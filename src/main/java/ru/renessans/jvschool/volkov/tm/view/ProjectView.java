package ru.renessans.jvschool.volkov.tm.view;

import ru.renessans.jvschool.volkov.tm.api.view.ICrudView;
import ru.renessans.jvschool.volkov.tm.constant.NotifyConst;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyProjectException;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public final class ProjectView implements ICrudView<Project> {

    @Override
    public String getLine() {
        System.out.println(NotifyConst.ADD_DATA_MSG);
        return ScannerUtil.nextLine();
    }

    @Override
    public void print(final List<Project> projects) {
        if (ValidRuleUtil.isNullOrEmpty(projects)) {
            System.out.printf((NotifyConst.EMPTY_LIST_MSG) + "%n", "проектов");
            return;
        }
        final AtomicInteger index = new AtomicInteger(1);
        projects.forEach(task -> {
            System.out.println(index + ". " + task + "\n");
            index.getAndIncrement();
        });
        System.out.println(NotifyConst.SUCCESS_RESULT_MSG);
    }

    @Override
    public void print(final Project project) {
        if (Objects.isNull(project)) {
            printFail();
            throw new EmptyProjectException();
        }
        System.out.printf("\n%s;\nИдентификатор: %s.\n%s",
                project, project.getId(), NotifyConst.SUCCESS_RESULT_MSG);
    }

    @Override
    public void printFail() {
        System.out.println(NotifyConst.FAIL_RESULT_MSG);
    }

}