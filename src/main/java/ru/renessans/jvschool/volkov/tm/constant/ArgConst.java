package ru.renessans.jvschool.volkov.tm.constant;

public interface ArgConst {

    String HELP = "-h";

    String VERSION = "-v";

    String ABOUT = "-a";

    String INFO = "-i";

    String ARGUMENTS = "-arg";

    String COMMANDS = "-cmd";

}