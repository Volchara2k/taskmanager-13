package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.model.Project;

import java.util.List;

public interface ICrudService<T> {

    T add(String title, String description);

    T updateByIndex(Integer index, String title, String description);

    T updateById(String id, String title, String description);

    T removeByIndex(Integer index);

    T removeById(String id);

    T removeByTitle(String title);

    List<T> removeAll();

    T getByIndex(Integer index);

    T getById(String id);

    T getByTitle(String title);

    List<T> getAll();

}