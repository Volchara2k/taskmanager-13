package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.enumeration.CommandType;
import ru.renessans.jvschool.volkov.tm.model.Command;

import java.util.List;

public interface ICommandService {

    List<CommandType> addCommandTypes();

    List<Command> addCommonCommands();

    List<Command> addTerminalCommands();

    List<Command> addArgumentCommands();

    void addNotifyCommonCommands();

    void addNotifyTerminalCommands();

    void addNotifyArgumentCommands();

    String getTerminalCommandNotify(String command);

    String getArgumentCommandNotify(String command);

}