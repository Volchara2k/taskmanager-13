package ru.renessans.jvschool.volkov.tm.view;

import ru.renessans.jvschool.volkov.tm.api.view.ICrudView;
import ru.renessans.jvschool.volkov.tm.constant.NotifyConst;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyTaskException;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public final class TaskView implements ICrudView<Task> {

    @Override
    public String getLine() {
        System.out.println(NotifyConst.ADD_DATA_MSG);
        return ScannerUtil.nextLine();
    }

    @Override
    public void print(final List<Task> tasks) {
        if (ValidRuleUtil.isNullOrEmpty(tasks)) {
            System.out.printf((NotifyConst.EMPTY_LIST_MSG) + "%n", "задач");
            return;
        }
        final AtomicInteger index = new AtomicInteger(1);
        tasks.forEach(task -> {
            System.out.print(index + ". " + task + "\n");
            index.getAndIncrement();
        });
        System.out.println(NotifyConst.SUCCESS_RESULT_MSG);
    }

    @Override
    public void print(final Task task) {
        if (Objects.isNull(task)) {
            printFail();
            throw new EmptyTaskException();
        }
        System.out.printf("\n%s;\nИдентификатор: %s.\n%s",
                task, task.getId(), NotifyConst.SUCCESS_RESULT_MSG);
    }

    @Override
    public void printFail() {
        System.out.println(NotifyConst.FAIL_RESULT_MSG);
    }

}