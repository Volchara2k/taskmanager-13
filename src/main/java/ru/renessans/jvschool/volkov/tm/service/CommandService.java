package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;
import ru.renessans.jvschool.volkov.tm.enumeration.CommandType;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyCommandException;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyCommandTypeException;
import ru.renessans.jvschool.volkov.tm.exception.unknown.UnknownCommandException;
import ru.renessans.jvschool.volkov.tm.model.Command;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public List<CommandType> addCommandTypes() {
        return this.commandRepository.addCommandTypes(
                Arrays.asList(
                        CommandType.COMMON, CommandType.CMD, CommandType.ARG
                )
        );
    }

    @Override
    public List<Command> addCommonCommands() {
        return this.commandRepository.addCommands(
                CommandType.COMMON,
                Arrays.asList(
                        Command.HELP, Command.VERSION, Command.ABOUT, Command.INFO, Command.ARGUMENT, Command.COMMAND,
                        Command.PROJECT_CREATE, Command.PROJECT_LIST, Command.PROJECT_CLEAR, Command.PROJECT_UPDATE_BY_INDEX,
                        Command.PROJECT_UPDATE_BY_ID, Command.PROJECT_DELETE_BY_INDEX, Command.PROJECT_DELETE_BY_ID,
                        Command.PROJECT_DELETE_BY_TITLE, Command.PROJECT_VIEW_BY_INDEX, Command.PROJECT_VIEW_BY_ID,
                        Command.TASK_CREATE, Command.TASK_LIST, Command.TASK_CLEAR, Command.TASK_UPDATE_BY_INDEX,
                        Command.TASK_UPDATE_BY_ID, Command.TASK_DELETE_BY_INDEX, Command.TASK_DELETE_BY_ID,
                        Command.TASK_DELETE_BY_TITLE, Command.TASK_VIEW_BY_INDEX, Command.TASK_VIEW_BY_ID, Command.EXIT
                )
        );
    }

    @Override
    public List<Command> addTerminalCommands() {
        return this.commandRepository.addCommands(
                CommandType.CMD,
                Arrays.asList(
                        Command.HELP, Command.VERSION, Command.ABOUT, Command.INFO, Command.ARGUMENT, Command.COMMAND,
                        Command.PROJECT_CREATE, Command.PROJECT_LIST, Command.PROJECT_CLEAR, Command.PROJECT_UPDATE_BY_INDEX,
                        Command.PROJECT_UPDATE_BY_ID, Command.PROJECT_DELETE_BY_INDEX, Command.PROJECT_DELETE_BY_ID,
                        Command.PROJECT_DELETE_BY_TITLE, Command.PROJECT_VIEW_BY_INDEX, Command.PROJECT_VIEW_BY_ID,
                        Command.TASK_CREATE, Command.TASK_LIST, Command.TASK_CLEAR, Command.TASK_UPDATE_BY_INDEX,
                        Command.TASK_UPDATE_BY_ID, Command.TASK_DELETE_BY_INDEX, Command.TASK_DELETE_BY_ID,
                        Command.TASK_DELETE_BY_TITLE, Command.TASK_VIEW_BY_INDEX, Command.TASK_VIEW_BY_ID, Command.EXIT
                )
        );
    }

    @Override
    public List<Command> addArgumentCommands() {
        return this.commandRepository.addCommands(
                CommandType.ARG,
                Arrays.asList(
                        Command.HELP, Command.VERSION, Command.ABOUT,
                        Command.INFO, Command.ARGUMENT, Command.COMMAND
                )
        );
    }

    @Override
    public void addNotifyCommonCommands() {
        final AtomicReference<String> commonNotify = new AtomicReference<>("");
        final List<Command> commons = this.commandRepository.getCommandsByType(CommandType.COMMON);
        commons.forEach(command -> {
            commonNotify.updateAndGet(v -> v + command.toString() + "\n");
            Command.HELP.setNotification(commonNotify.get());
        });
    }

    @Override
    public void addNotifyTerminalCommands() {
        final AtomicReference<String> argumentNotify = new AtomicReference<>("");
        final List<Command> commands = this.commandRepository.getCommandsByType(CommandType.CMD);
        commands.forEach(command -> {
            argumentNotify.updateAndGet(v -> v + command.getCommand() + "\n");
            Command.COMMAND.setNotification(argumentNotify.get());
        });
    }

    @Override
    public void addNotifyArgumentCommands() {
        final AtomicReference<String> commandNotify = new AtomicReference<>("");
        final List<Command> arguments = this.commandRepository.getCommandsByType(CommandType.ARG);
        arguments.forEach(command -> {
            commandNotify.updateAndGet(v -> v + command.getArgument() + "\n");
            Command.ARGUMENT.setNotification(commandNotify.get());
        });
    }

    @Override
    public String getTerminalCommandNotify(final String command) {
        if (ValidRuleUtil.isNullOrEmpty(command)) throw new EmptyCommandException();
        return notifyByType(CommandType.CMD, command);
    }

    @Override
    public String getArgumentCommandNotify(final String command) {
        if (ValidRuleUtil.isNullOrEmpty(command)) throw new EmptyCommandException();
        return notifyByType(CommandType.ARG, command);
    }

    private String notifyByType(final CommandType commandType, final String command) {
        if (Objects.isNull(commandType)) throw new EmptyCommandTypeException();
        if (ValidRuleUtil.isNullOrEmpty(command)) throw new EmptyCommandException();

        String notify = "";
        if (commandType.isCommon())
            notify = this.commandRepository.getNotifyByType(CommandType.COMMON, command);
        if (commandType.isArgument())
            notify = this.commandRepository.getNotifyByType(CommandType.ARG, command);
        if (commandType.isCommand())
            notify = this.commandRepository.getNotifyByType(CommandType.CMD, command);
        if (ValidRuleUtil.isNullOrEmpty(notify)) throw new UnknownCommandException(command);
        return notify;
    }

}