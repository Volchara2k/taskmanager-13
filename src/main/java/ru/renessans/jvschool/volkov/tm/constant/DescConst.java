package ru.renessans.jvschool.volkov.tm.constant;

public interface DescConst {

    String HELP = "вывод списка команд";

    String VERSION = "вывод версии программы";

    String ABOUT = "вывод информации о разработчике";

    String INFO = "вывод информации о системе";

    String ARGUMENTS = "вывод списка поддерживаемых аргументов";

    String COMMANDS = "вывод списка поддерживаемых терминальных команд";

    String EXIT = "закрыть приложение";

    String TASK_CREATE = "добавить новую задачу";

    String TASK_LIST = "вывод списка задач";

    String TASK_CLEAR = "очистить все задачи";

    String TASK_UPDATE_BY_INDEX = "обновить задачу по индексу";

    String TASK_UPDATE_BY_ID = "обновить задачу по идентификатору";

    String TASK_DELETE_BY_INDEX = "удалить задачу по индексу";

    String TASK_DELETE_BY_ID = "удалить задачу по идентификатору";

    String TASK_DELETE_BY_TITLE = "удалить задачу по заголовку";

    String TASK_VIEW_BY_INDEX = "просмотреть задачу по индексу";

    String TASK_VIEW_BY_ID = "просмотреть задачу по идентификатору";

    String PROJECT_CREATE = "добавить новый проект";

    String PROJECT_LIST = "вывод списка проектов";

    String PROJECT_CLEAR = "очистить все проекты";

    String PROJECT_UPDATE_BY_INDEX = "обновить проект по индексу";

    String PROJECT_UPDATE_BY_ID = "обновить проект по идентификатору";

    String PROJECT_DELETE_BY_INDEX = "удалить проект по индексу";

    String PROJECT_DELETE_BY_ID = "удалить проект по идентификатору";

    String PROJECT_DELETE_BY_TITLE = "удалить проект по заголовку";

    String PROJECT_VIEW_BY_INDEX = "просмотреть проект по индексу";

    String PROJECT_VIEW_BY_ID = "просмотреть проект по идентификатору";

}