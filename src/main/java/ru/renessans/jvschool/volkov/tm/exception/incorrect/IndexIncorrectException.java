package ru.renessans.jvschool.volkov.tm.exception.incorrect;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class IndexIncorrectException extends AbstractRuntimeException {

    private static final String INDEX_NOT_INTEGER =
            "Ошибка! Значение \"%s\" параметра \"индекс\" не является целочисленным типом данных!\n";

    private static final String INDEX_INCORRECT = "Ошибка! Парамерт \"индекс\" является некорректным!\n";

    public IndexIncorrectException() {
        super(INDEX_INCORRECT);
    }

    public IndexIncorrectException(final String message) {
        super(String.format(INDEX_NOT_INTEGER, message));
    }

}