package ru.renessans.jvschool.volkov.tm.exception.unknown;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public class UnknownCommandTypeException extends AbstractRuntimeException {

    private static final String UNKNOWN_COMMAND_TYPE = "Ошибка! Неизвестных тип команды: \"%s\"!\n";

    public UnknownCommandTypeException() {
        super(UNKNOWN_COMMAND_TYPE);
    }

}