package ru.renessans.jvschool.volkov.tm.exception.empty;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyCommandException extends AbstractRuntimeException {

    private static final String EMPTY_COMMAND = "Ошибка! Парамерт \"команда\" является пустым или null!\n";

    public EmptyCommandException() {
        super(EMPTY_COMMAND);
    }

}